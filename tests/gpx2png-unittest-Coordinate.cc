/* This file is part of the 'gpx2png' program
 * (https://framagit.org/groolot/gpx2png-ng)
 *
 * Copyright (C) 2021 Grégory David <groolot@groolot.net>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
*/

#include "Coordinate.h"

#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#include <log4cxx/logger.h>
#include <log4cxx/logmanager.h>
#include <log4cxx/basicconfigurator.h>

#include "config.h"
#define PROGNAME PACKAGE"-unittest-Coordinate"

static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger(PACKAGE));
static log4cxx::LoggerPtr local_logger(log4cxx::Logger::getLogger(PROGNAME));

class test_Coordinate : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(test_Coordinate);
  CPPUNIT_TEST(testCoordinate_constructors);
  CPPUNIT_TEST(testCoordinate_operators);
  CPPUNIT_TEST(testCoordinate_limites);
  CPPUNIT_TEST_SUITE_END();

 public:
  void setUp() final
  {
    if(!log4cxx::LogManager::getLoggerRepository()->isConfigured())
    {
      log4cxx::BasicConfigurator::configure();
    }

    local_logger->setLevel(log4cxx::Level::getDebug());
  };

  void tearDown() final
  {};

  void testCoordinate_constructors()
  {
    CPPUNIT_ASSERT_NO_THROW(gps::Coordinate());
    CPPUNIT_ASSERT_NO_THROW(gps::Coordinate(1, 2));
    gps::Coordinate *coordinate;
    CPPUNIT_ASSERT_NO_THROW(coordinate = new gps::Coordinate(gps::Coordinate(6, 7)));
    CPPUNIT_ASSERT_EQUAL(static_cast<double>(6),
                         coordinate->latitude());
    CPPUNIT_ASSERT_EQUAL(static_cast<double>(7),
                         coordinate->longitude());
  };

  void testCoordinate_operators()
  {
    gps::Coordinate coordinate1(1, 2);
    gps::Coordinate coordinate2 = coordinate1;
    CPPUNIT_ASSERT_EQUAL(coordinate1.latitude(),
                         coordinate2.latitude());
    CPPUNIT_ASSERT_EQUAL(coordinate1.longitude(),
                         coordinate2.longitude());
    CPPUNIT_ASSERT(coordinate1 == coordinate2);
    CPPUNIT_ASSERT(coordinate1 <= coordinate2);
    coordinate2.longitude(100);
    CPPUNIT_ASSERT(coordinate1 <= coordinate2);
    CPPUNIT_ASSERT(coordinate1 < coordinate2);
    CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(coordinate1 > coordinate2));
    CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(coordinate1 >= coordinate2));
  };

  void testCoordinate_limites()
  {
    gps::Coordinate *coordinate = new gps::Coordinate();
    // Latitude
    CPPUNIT_ASSERT_NO_THROW(coordinate->latitude(0));
    CPPUNIT_ASSERT_NO_THROW(coordinate->latitude(90));
    CPPUNIT_ASSERT_NO_THROW(coordinate->latitude(-90));
    CPPUNIT_ASSERT_THROW(coordinate->latitude(90.000001),
                         gps::LatitudeLimite);
    CPPUNIT_ASSERT_THROW(coordinate->latitude(-90.000001),
                         gps::LatitudeLimite);
    // Longitude
    CPPUNIT_ASSERT_NO_THROW(coordinate->longitude(0));
    CPPUNIT_ASSERT_NO_THROW(coordinate->longitude(180));
    CPPUNIT_ASSERT_NO_THROW(coordinate->longitude(-180));
    CPPUNIT_ASSERT_THROW(coordinate->longitude(180.000001),
                         gps::LongitudeLimite);
    CPPUNIT_ASSERT_THROW(coordinate->longitude(-180.000001),
                         gps::LongitudeLimite);
    delete coordinate;
  };

};

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION(test_Coordinate);

int main()
{
  // Get the top level suite from the registry
  CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry();
  CppUnit::Test *suite = registry.makeTest();
  // Adds the test to the list of test to run
  CppUnit::TextUi::TestRunner runner;
  runner.addTest(suite);
  // Change the default outputter to a compiler error format outputter
  runner.setOutputter(new CppUnit::CompilerOutputter(&runner.result(),
                      std::cerr));
  // Run the tests.
  bool wasSucessful = runner.run();
  // Return error code 1 if the one of test failed.
  return wasSucessful ? 0 : 1;
}
