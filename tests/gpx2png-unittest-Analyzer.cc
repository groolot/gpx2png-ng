/* This file is part of the 'gpx2png' program
 * (https://framagit.org/groolot/gpx2png-ng)
 *
 * Copyright (C) 2021 Grégory David <groolot@groolot.net>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
*/

#include "Analyzer.h"

#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#include <log4cxx/logger.h>
#include <log4cxx/logmanager.h>
#include <log4cxx/basicconfigurator.h>

#include "config.h"
#define PROGNAME PACKAGE"-unittest-cli"

static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger(PACKAGE));
static log4cxx::LoggerPtr local_logger(log4cxx::Logger::getLogger(PROGNAME));

class test_Analyzer : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(test_Analyzer);
  CPPUNIT_TEST(testInstanceSingleton);
  CPPUNIT_TEST(testParser_quiet_flag);
  CPPUNIT_TEST(testParser_quiet_flag_long);
  CPPUNIT_TEST(testParser_loglevel_flag);
  CPPUNIT_TEST(testParser_loglevel_flag_long);
  CPPUNIT_TEST(testParser_loglevel_flag_default_value);
  CPPUNIT_TEST(testParser_verbose_flag);
  CPPUNIT_TEST(testParser_verbose_flag_long);
  CPPUNIT_TEST(testParser_verbose_flag_above);
  CPPUNIT_TEST(testParser_tilesdir_flag);
  CPPUNIT_TEST(testParser_tilesdir_flag_long);
  CPPUNIT_TEST(testParser_tilesdir_flag_default_value);
  CPPUNIT_TEST(testParser_output_flag);
  CPPUNIT_TEST(testParser_output_flag_long);
  CPPUNIT_TEST(testParser_output_flag_default_value);
  CPPUNIT_TEST(testParser_zoom_flag);
  CPPUNIT_TEST(testParser_zoom_flag_exception);
  CPPUNIT_TEST(testParser_zoom_flag_long);
  CPPUNIT_TEST(testParser_zoom_flag_default_value);
  CPPUNIT_TEST(testParser_max_number_tiles_autozoom_flag);
  CPPUNIT_TEST(testParser_max_number_tiles_autozoom_flag_exception);
  CPPUNIT_TEST(testParser_max_number_tiles_autozoom_flag_long);
  CPPUNIT_TEST(testParser_max_number_tiles_autozoom_flag_default_value);
  CPPUNIT_TEST(testParser_bordertiles_flag);
  CPPUNIT_TEST(testParser_bordertiles_flag_exception);
  CPPUNIT_TEST(testParser_bordertiles_flag_long);
  CPPUNIT_TEST(testParser_bordertiles_flag_default_value);
  CPPUNIT_TEST(testParser_cutborder_flag);
  CPPUNIT_TEST(testParser_cutborder_flag_exception);
  CPPUNIT_TEST(testParser_cutborder_flag_long);
  CPPUNIT_TEST(testParser_cutborder_flag_default_value);
  CPPUNIT_TEST(testParser_waypoint_radius_flag);
  CPPUNIT_TEST(testParser_waypoint_radius_flag_exception);
  CPPUNIT_TEST(testParser_waypoint_radius_flag_long);
  CPPUNIT_TEST(testParser_waypoint_radius_flag_default_value);
  CPPUNIT_TEST(testParser_waypoint_color_flag_regular);
  CPPUNIT_TEST(testParser_waypoint_color_flag_caps);
  CPPUNIT_TEST(testParser_waypoint_color_flag_exception);
  CPPUNIT_TEST(testParser_waypoint_color_flag_default_value);
  CPPUNIT_TEST(testParser_linewidth_flag);
  CPPUNIT_TEST(testParser_linewidth_flag_exception);
  CPPUNIT_TEST(testParser_linewidth_flag_long);
  CPPUNIT_TEST(testParser_linewidth_flag_default_value);
  CPPUNIT_TEST(testParser_animate_flag);
  CPPUNIT_TEST(testParser_animate_flag_long);
  CPPUNIT_TEST(testParser_animate_flag_default_value);
  CPPUNIT_TEST(testParser_tiles_flag);
  CPPUNIT_TEST(testParser_tiles_flag_long);
  CPPUNIT_TEST(testParser_tiles_flag_default_value);
  CPPUNIT_TEST(testParser_backgroundpostprocess_flag);
  CPPUNIT_TEST(testParser_backgroundpostprocess_flag_exception);
  CPPUNIT_TEST(testParser_backgroundpostprocess_flag_default_value);
  CPPUNIT_TEST(testParser_sparse_flag);
  CPPUNIT_TEST(testParser_sparse_flag_long);
  CPPUNIT_TEST(testParser_sparse_flag_default_value);
  CPPUNIT_TEST(testParser_thumbnail_flag);
  CPPUNIT_TEST(testParser_thumbnail_flag_long);
  CPPUNIT_TEST(testParser_thumbnailsize_flag);
  CPPUNIT_TEST(testParser_thumbnailsize_flag_long);
  CPPUNIT_TEST(testParser_thumbnailsize_flag_default_value);
  CPPUNIT_TEST(testParser_invisiblewaypoint_flag);
  CPPUNIT_TEST(testParser_invisiblewaypoint_flag_long);
  CPPUNIT_TEST(testParser_boundingbox_flag);
  CPPUNIT_TEST(testParser_boundingbox_flag_long);
  CPPUNIT_TEST(testParser_boundingbox_bad_order);
  CPPUNIT_TEST_SUITE_END();

 private:
  cli::Analyzer *cli;

 public:
  void setUp()
  {
    cli = cli::Analyzer::instance();
    cli->setExceptionHandling(false);

    if(!log4cxx::LogManager::getLoggerRepository()->isConfigured())
    {
      log4cxx::BasicConfigurator::configure();
    }

    local_logger->setLevel(log4cxx::Level::getDebug());
  };

  void tearDown()
  {
    cli->deleteInstance();
  };

  void testInstanceSingleton()
  {
    cli::Analyzer *cli_second = cli::Analyzer::instance();
    CPPUNIT_ASSERT_EQUAL(cli, cli_second);
  };

  /// Tests for --quiet|-q
  void testParser_quiet_flag()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("-q");
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(logger->getLevel(),
                         log4cxx::Level::getFatal());
  };

  void testParser_quiet_flag_long()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("--quiet");
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(logger->getLevel(),
                         log4cxx::Level::getFatal());
  };

  /// Tests for --loglevel|-l
  void testParser_loglevel_flag()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("-l");
    argv.push_back("WARN");
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(logger->getLevel(),
                         log4cxx::Level::getWarn());
  };

  void testParser_loglevel_flag_long()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("--loglevel");
    argv.push_back("FATAL");
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(logger->getLevel(),
                         log4cxx::Level::getFatal());
  };

  void testParser_loglevel_flag_default_value()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(logger->getLevel(),
                         log4cxx::Level::toLevel(DEFAULT_LOGLEVEL));
  };

  /// Tests for --verbose|-v
  void testParser_verbose_flag()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("-v");
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(logger->getLevel(),
                         log4cxx::Level::getWarn());
  };

  void testParser_verbose_flag_long()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("--verbose");
    argv.push_back("--verbose");
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(logger->getLevel(),
                         log4cxx::Level::getInfo());
  };

  void testParser_verbose_flag_above()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("-vvvvvvvvvvvvv");
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(logger->getLevel(),
                         log4cxx::Level::getDebug());
  };

  /// Tests for --tilesdir|-T
  void testParser_tilesdir_flag()
  {
    std::string option("/tmp/check/tiles/directory/short");
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("-T");
    argv.push_back(const_cast<char *>(option.c_str()));
    cli->parse(argv);
    CPPUNIT_ASSERT(cli->getTilesDirectory() == option);
  };

  void testParser_tilesdir_flag_long()
  {
    std::string option("/tmp/check/tiles/directory/long");
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("--tilesdir");
    argv.push_back(const_cast<char *>(option.c_str()));
    cli->parse(argv);
    CPPUNIT_ASSERT(cli->getTilesDirectory() == option);
  };

  void testParser_tilesdir_flag_default_value()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    cli->parse(argv);
    CPPUNIT_ASSERT(cli->getTilesDirectory() == std::string(DEFAULT_TILES_DIRECTORY));
  };

  /// Tests for --output|-o
  void testParser_output_flag()
  {
    std::string option("/path/to/output_filename_short");
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("-o");
    argv.push_back(const_cast<char *>(option.c_str()));
    cli->parse(argv);
    CPPUNIT_ASSERT(cli->getOutputFilename() == option);
  };

  void testParser_output_flag_long()
  {
    std::string option("/path/to/output_filename_long");
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("--output");
    argv.push_back(const_cast<char *>(option.c_str()));
    cli->parse(argv);
    CPPUNIT_ASSERT(cli->getOutputFilename() == option);
  };

  void testParser_output_flag_default_value()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    cli->parse(argv);
    CPPUNIT_ASSERT(cli->getOutputFilename() == std::string(DEFAULT_OUTPUT_FILENAME));
  };

  /// Tests for --zoom|-z
  void testParser_zoom_flag()
  {
    std::string option(std::to_string(DEFAULT_ZOOM_MAX));
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("-z");
    argv.push_back(const_cast<char *>(option.c_str()));
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(std::uint16_t(DEFAULT_ZOOM_MAX),
                         cli->getZoom());
  };

  void testParser_zoom_flag_exception()
  {
    std::string option(std::to_string(DEFAULT_ZOOM_MAX + 100));
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("-z");
    argv.push_back(const_cast<char *>(option.c_str()));
    CPPUNIT_ASSERT_THROW(cli->parse(argv),
                         TCLAP::ArgException);
  };

  void testParser_zoom_flag_long()
  {
    std::string option(std::to_string(DEFAULT_ZOOM_MIN));
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("--zoom");
    argv.push_back(option);
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(std::uint16_t(DEFAULT_ZOOM_MIN),
                         cli->getZoom());
  };

  void testParser_zoom_flag_default_value()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(std::uint16_t(DEFAULT_ZOOM),
                         cli->getZoom());
  };

  /// Tests for --autozoom|-a
  void testParser_max_number_tiles_autozoom_flag()
  {
    std::string option(std::to_string(DEFAULT_MAX_NUMBER_TILES_AUTOZOOM_MAX));
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("-a");
    argv.push_back(option);
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(std::uint16_t(DEFAULT_MAX_NUMBER_TILES_AUTOZOOM_MAX),
                         cli->getMaxNumberTilesAutozoom());
  };

  void testParser_max_number_tiles_autozoom_flag_exception()
  {
    std::string option(std::to_string(DEFAULT_MAX_NUMBER_TILES_AUTOZOOM_MAX + 1));
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("-a");
    argv.push_back(option);
    CPPUNIT_ASSERT_THROW(cli->parse(argv),
                         TCLAP::ArgException);
  };

  void testParser_max_number_tiles_autozoom_flag_long()
  {
    std::string option(std::to_string(DEFAULT_MAX_NUMBER_TILES_AUTOZOOM));
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("--autozoom");
    argv.push_back(option);
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(std::uint16_t(DEFAULT_MAX_NUMBER_TILES_AUTOZOOM),
                         cli->getMaxNumberTilesAutozoom());
  };

  void testParser_max_number_tiles_autozoom_flag_default_value()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(std::uint16_t(DEFAULT_MAX_NUMBER_TILES_AUTOZOOM),
                         cli->getMaxNumberTilesAutozoom());
  };

  /// Tests for --bordertiles|-b
  void testParser_bordertiles_flag()
  {
    std::string option(std::to_string(DEFAULT_BORDERTILES_MAX));
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("-b");
    argv.push_back(option);
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(std::uint16_t(DEFAULT_BORDERTILES_MAX),
                         cli->getBorderTiles());
  };

  void testParser_bordertiles_flag_exception()
  {
    std::string option(std::to_string(DEFAULT_BORDERTILES_MAX + 1));
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("-b");
    argv.push_back(option);
    CPPUNIT_ASSERT_THROW(cli->parse(argv),
                         TCLAP::ArgException);
  };

  void testParser_bordertiles_flag_long()
  {
    std::string option("0");
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("--bordertiles");
    argv.push_back(option);
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(std::uint16_t(0),
                         cli->getBorderTiles());
  };

  void testParser_bordertiles_flag_default_value()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(std::uint16_t(DEFAULT_BORDERTILES),
                         cli->getBorderTiles());
  };

  /// Tests for --cutborder|-c
  void testParser_cutborder_flag()
  {
    std::string option(std::to_string(DEFAULT_CUTBORDER_MIN));
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("-c");
    argv.push_back(option);
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(std::uint16_t(DEFAULT_CUTBORDER_MIN),
                         cli->getCutBorder());
  };

  void testParser_cutborder_flag_exception()
  {
    std::string option(std::to_string(DEFAULT_CUTBORDER_MAX + 1));
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("-c");
    argv.push_back(option);
    CPPUNIT_ASSERT_THROW(cli->parse(argv),
                         TCLAP::ArgException);
  };

  void testParser_cutborder_flag_long()
  {
    std::string option(std::to_string(DEFAULT_CUTBORDER_MAX));
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("--cutborder");
    argv.push_back(option);
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(std::uint16_t(DEFAULT_CUTBORDER_MAX),
                         cli->getCutBorder());
  };

  void testParser_cutborder_flag_default_value()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(std::uint16_t(DEFAULT_CUTBORDER),
                         cli->getCutBorder());
  };

  /// Tests for --waypointradius|-r
  void testParser_waypoint_radius_flag()
  {
    std::uint16_t option(123);
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("-r");
    argv.push_back(std::to_string(option));
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(option,
                         cli->getWaypointRadius());
  };

  void testParser_waypoint_radius_flag_exception()
  {
    std::uint16_t option(DEFAULT_WAYPOINT_RADIUS_MAX + 1);
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("-r");
    argv.push_back(std::to_string(option));
    CPPUNIT_ASSERT_THROW(cli->parse(argv),
                         TCLAP::ArgException);
  };

  void testParser_waypoint_radius_flag_long()
  {
    std::uint16_t option(123);
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("--waypointradius");
    argv.push_back(std::to_string(option));
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(option,
                         cli->getWaypointRadius());
  };

  void testParser_waypoint_radius_flag_default_value()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(std::uint16_t(DEFAULT_WAYPOINT_RADIUS),
                         cli->getWaypointRadius());
  };

  /// Tests for --waypointcolor
  void testParser_waypoint_color_flag_regular()
  {
    std::string option("#deadbeef");
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("--waypointcolor");
    argv.push_back(option);
    cli->parse(argv);
    CPPUNIT_ASSERT(option == cli->getWayPointColor());
  };

  void testParser_waypoint_color_flag_caps()
  {
    std::string option("#DEADBEEF");
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("--waypointcolor");
    argv.push_back(option);
    cli->parse(argv);
    CPPUNIT_ASSERT(option == cli->getWayPointColor());
  };

  void testParser_waypoint_color_flag_exception()
  {
    std::string option("#deadbeefs");
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("--waypointcolor");
    argv.push_back(option);
    CPPUNIT_ASSERT_THROW(cli->parse(argv),
                         TCLAP::ArgException);
  };

  void testParser_waypoint_color_flag_default_value()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    cli->parse(argv);
    CPPUNIT_ASSERT(std::string(DEFAULT_WAYPOINT_COLOR) == cli->getWayPointColor());
  };

  /// Tests for --linewidth|-w
  void testParser_linewidth_flag()
  {
    std::uint16_t option(DEFAULT_LINE_WIDTH_MIN);
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("-w");
    argv.push_back(std::to_string(option));
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(option,
                         cli->getLineWidth());
  };

  void testParser_linewidth_flag_exception()
  {
    std::uint16_t option(DEFAULT_LINE_WIDTH_MAX + 1);
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("-w");
    argv.push_back(std::to_string(option));
    CPPUNIT_ASSERT_THROW(cli->parse(argv),
                         TCLAP::ArgException);
  };

  void testParser_linewidth_flag_long()
  {
    std::uint16_t option(DEFAULT_LINE_WIDTH_MAX);
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("--linewidth");
    argv.push_back(std::to_string(option));
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(option,
                         cli->getLineWidth());
  };

  void testParser_linewidth_flag_default_value()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(std::uint16_t(DEFAULT_LINE_WIDTH),
                         cli->getLineWidth());
  };

  /// Tests for --animate|-A
  void testParser_animate_flag()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("-A");
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(true,
                         cli->getAnimate());
  };

  void testParser_animate_flag_long()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("--animate");
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(true,
                         cli->getAnimate());
  };

  void testParser_animate_flag_default_value()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(false,
                         cli->getAnimate());
  };

  /// Tests for --tiles|-t
  void testParser_tiles_flag()
  {
    std::string option("opentopo");
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("-t");
    argv.push_back(option);
    cli->parse(argv);
    CPPUNIT_ASSERT(cli->getTiles() == option);
  };

  void testParser_tiles_flag_long()
  {
    std::string option("hikebike");
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("--tiles");
    argv.push_back(option);
    cli->parse(argv);
    CPPUNIT_ASSERT(cli->getTiles() == option);
  };

  void testParser_tiles_flag_default_value()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    cli->parse(argv);
    CPPUNIT_ASSERT(cli->getTiles() == std::string(DEFAULT_TILES));
  };

  /// Tests for --backgroundpostprocess
  void testParser_backgroundpostprocess_flag()
  {
    std::string option("brightgrey");
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("--backgroundpostprocess");
    argv.push_back(option);
    cli->parse(argv);
    CPPUNIT_ASSERT(cli->getBackgroundPostProcess() == option);
  };

  void testParser_backgroundpostprocess_flag_exception()
  {
    std::string option("Undefined");
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("--backgroundpostprocess");
    argv.push_back(option);
    CPPUNIT_ASSERT_THROW(cli->parse(argv),
                         TCLAP::ArgException);
  };

  void testParser_backgroundpostprocess_flag_default_value()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    cli->parse(argv);
    CPPUNIT_ASSERT(cli->getBackgroundPostProcess() == std::string(DEFAULT_BACKGROUND_POST_PROCESS));
  };

  /// Tests for --sparse|-s
  void testParser_sparse_flag()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("-s");
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(true,
                         cli->getSparse());
  };

  void testParser_sparse_flag_long()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("--sparse");
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(true,
                         cli->getSparse());
  };

  void testParser_sparse_flag_default_value()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(false,
                         cli->getSparse());
  };

  /// Tests for --thumbnailsize|-J
  void testParser_thumbnailsize_flag()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("-J");
    argv.push_back("50");
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(std::uint16_t(50),
                         cli->getThumbnailSize());
  };

  void testParser_thumbnailsize_flag_long()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("--thumbnailsize");
    argv.push_back("100");
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(std::uint16_t(100),
                         cli->getThumbnailSize());
  };

  void testParser_thumbnailsize_flag_default_value()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(std::uint16_t(DEFAULT_THUMBNAIL_SIZE),
                         cli->getThumbnailSize());
  };

  /// Tests for --thumbnail|-j
  void testParser_thumbnail_flag()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("-j");
    argv.push_back("photo1.jpg");
    argv.push_back("-j");
    argv.push_back("photo2.jpg");
    cli->parse(argv);
    CPPUNIT_ASSERT(cli->getThumbnails().back() == std::string("photo2.jpg"));
  };

  void testParser_thumbnail_flag_long()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("--thumbnail");
    argv.push_back("photoA.jpg");
    argv.push_back("--thumbnail");
    argv.push_back("photoB.jpg");
    cli->parse(argv);
    CPPUNIT_ASSERT(cli->getThumbnails().back() == std::string("photoB.jpg"));
  };

  /// Tests for --invisiblewaypoint|-W
  void testParser_invisiblewaypoint_flag()
  {
    gps::WayPoint invisiblewaypoint(45.6789, 0.1234);
    std::string option(invisiblewaypoint.str().c_str());
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("-W");
    argv.push_back(option);
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(invisiblewaypoint.latitude(),
                         cli->getInvisibleWayPoints().front().latitude());
    CPPUNIT_ASSERT_EQUAL(invisiblewaypoint.longitude(),
                         cli->getInvisibleWayPoints().back().longitude());
  };

  void testParser_invisiblewaypoint_flag_exception()
  {
    gps::WayPoint invisiblewaypoint(45.6789, 0.1234);
    std::string option(invisiblewaypoint.str().c_str());
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("-W");
    argv.push_back(option);
    CPPUNIT_ASSERT_THROW(cli->parse(argv),
                         TCLAP::ArgException);
  };

  void testParser_invisiblewaypoint_flag_long()
  {
    gps::WayPoint invisiblewaypoint(45.6789, 0.1234);
    std::string option(invisiblewaypoint.str().c_str());
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("--invisiblewaypoint");
    argv.push_back(option);
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(invisiblewaypoint.longitude(),
                         cli->getInvisibleWayPoints().front().longitude());
    CPPUNIT_ASSERT_EQUAL(invisiblewaypoint.latitude(),
                         cli->getInvisibleWayPoints().back().latitude());
  };

  /// Tests for --boundingbox|-B
  void testParser_boundingbox_flag()
  {
    gps::BoundingBox boundingbox(45.67890, 0.12340, 45.67891, 0.12341);
    std::string option(boundingbox.str().c_str());
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("-B");
    argv.push_back(option);
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(boundingbox.left().latitude(),
                         cli->getBoundingBox().left().latitude());
    CPPUNIT_ASSERT_EQUAL(boundingbox.right().longitude(),
                         cli->getBoundingBox().right().longitude());
  };

  void testParser_boundingbox_flag_long()
  {
    gps::BoundingBox boundingbox(45.67890, 0.12340, 45.67891, 0.12341);
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("--boundingbox");
    argv.push_back(boundingbox.str());
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(boundingbox.left().longitude(),
                         cli->getBoundingBox().left().longitude());
    CPPUNIT_ASSERT_EQUAL(boundingbox.right().latitude(),
                         cli->getBoundingBox().right().latitude());
  };

  void testParser_boundingbox_bad_order()
  {
    std::vector<std::string> argv;
    argv.push_back(PROGNAME);
    argv.push_back("--boundingbox");
    argv.push_back("2.2:0.2:1.1:0.1");
    cli->parse(argv);
    CPPUNIT_ASSERT_EQUAL(static_cast<double>(2.2),
                         cli->getBoundingBox().right().latitude());
    CPPUNIT_ASSERT_EQUAL(static_cast<double>(0.2),
                         cli->getBoundingBox().right().longitude());
  };
};

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION(test_Analyzer);

int main()
{
  // Get the top level suite from the registry
  CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry();
  CppUnit::Test *suite = registry.makeTest();
  // Adds the test to the list of test to run
  CppUnit::TextUi::TestRunner runner;
  runner.addTest(suite);
  // Change the default outputter to a compiler error format outputter
  runner.setOutputter(new CppUnit::CompilerOutputter(&runner.result(),
                      std::cerr));
  // Run the tests.
  bool wasSucessful = runner.run();
  // Return error code 1 if the one of test failed.
  return wasSucessful ? 0 : 1;
}
