/* This file is part of the 'gpx2png' program
 * (https://framagit.org/groolot/gpx2png-ng)
 *
 * Copyright (C) 2021 Grégory David <groolot@groolot.net>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
*/

#include "BoundingBox.h"

#include <ostream>
#include <regex>

#include <log4cxx/logger.h>
#include <log4cxx/logmanager.h>
#include <log4cxx/basicconfigurator.h>
#include <tclap/ArgException.h>

#include "config.h"

static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger(PACKAGE));

gps::BoundingBox::BoundingBox() :
  left_(),
  right_()
{}

gps::BoundingBox::BoundingBox(gps::WayPoint _left, gps::WayPoint _right) :
  left_(_left),
  right_(_right)
{
  if(_left > _right)
  {
    gps::WayPoint tmp(_right);
    right_ = left_;
    left_ = tmp;
  }
  else if(_left == _right)
  {
    throw TCLAP::ArgParseException("both coordinates are similar ; please consider providing different points to make a bounding box (rectangle)");
  }
}

gps::BoundingBox::BoundingBox(double _left_lat, double _left_lon,
                              double _right_lat, double _right_lon) :
  BoundingBox(gps::WayPoint(_left_lat, _left_lon),
              gps::WayPoint(_right_lat, _right_lon))
{}

gps::BoundingBox::~BoundingBox()
{}

gps::BoundingBox &gps::BoundingBox::operator=(const std::string &input_string)
{
  std::regex boundingbox_syntax("^([+-]?(0|[1-9][0-9]*)\\.[0-9]+):([+-]?(0|[1-9][0-9]*)\\.[0-9]+):([+-]?(0|[1-9][0-9]*)\\.[0-9]+):([+-]?(0|[1-9][0-9]*)\\.[0-9]+)$");
  std::smatch boundingbox_submatches;

  if(std::regex_match(input_string,
                      boundingbox_submatches,
                      boundingbox_syntax)
    )
  {
    if(boundingbox_submatches.size() == 9)
    {
      gps::WayPoint left(std::stod(boundingbox_submatches[1].str()), std::stod(boundingbox_submatches[3].str()));
      gps::WayPoint right(std::stod(boundingbox_submatches[5].str()), std::stod(boundingbox_submatches[7].str()));
      *this = BoundingBox(left, right);
    }
  }
  else
  {
    throw TCLAP::ArgParseException(input_string + " does not validate BoundingBox format, please provide a ddd.ddd:ddd.ddd:ddd.ddd:ddd.ddd form");
  }

  return *this;
}

std::string gps::BoundingBox::str()
{
  std::ostringstream output;
  output << left_.str()
         << ":"
         << right_.str()
         ;
  return output.str();
}

gps::WayPoint gps::BoundingBox::left()
{
  return left_;
}

gps::WayPoint gps::BoundingBox::right()
{
  return right_;
}
