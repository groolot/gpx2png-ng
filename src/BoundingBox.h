/* This file is part of the 'gpx2png' program
 * (https://framagit.org/groolot/gpx2png-ng)
 *
 * Copyright (C) 2021 Grégory David <groolot@groolot.net>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
*/

#ifndef GPX2PNG_SRC_BOUNDINGBOX_H_
#define GPX2PNG_SRC_BOUNDINGBOX_H_

#include "WayPoint.h"

#include <cstdint>
#include <string>
#include <vector>

namespace gps {
/** \class BoundingBox
 *
 */
class BoundingBox : public TCLAP::StringLikeTrait
{
 public:
  BoundingBox();
  BoundingBox(gps::WayPoint _left, gps::WayPoint _right);
  BoundingBox(double _left_lat, double _left_lon, double _right_lat, double _right_lon);
  ~BoundingBox();
  BoundingBox &operator=(const std::string &input_string);
  std::string str();
  gps::WayPoint left();
  gps::WayPoint right();

 private:
  gps::WayPoint left_;
  gps::WayPoint right_;
};
}

#endif  // GPX2PNG_SRC_BOUNDINGBOX_H_
