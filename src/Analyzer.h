/* This file is part of the 'gpx2png' program
 * (https://framagit.org/groolot/gpx2png-ng)
 *
 * Copyright (C) 2021 Grégory David <groolot@groolot.net>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
*/

#ifndef GPX2PNG_SRC_ANALYZER_H_
#define GPX2PNG_SRC_ANALYZER_H_

#include "AnalyzerConstraints.h"
#include "WayPoint.h"
#include "BoundingBox.h"
#include "HTMLColorTrait.h"

#include <tclap/CmdLine.h>

#include <cstdint>
#include <string>
#include <vector>

namespace cli {
/** \class Analyzer
 *
 * \brief Command Line Interface (CLI) analyzer
 *
 * This class is an helper to the CLI management. It contains options
 * and parameters waited on CLI, directly or through other classes
 * like Criteria or Configuration.
 *
 * \note
 * Implements singleton design pattern
 */
class Analyzer
{
 private:
  static Analyzer *singleton;  ///< Pointer to the static singleton class member
  TCLAP::CmdLine cmd;

  // TCLAP Arguments pointers
  TCLAP::ValuesConstraint<std::string> *allowed_loglevels;  ///< TCLAP Container for allowed log levels
  TCLAP::SwitchArg *quiet_Arg;  ///< Quiet flag to set log level
  TCLAP::ValueArg<std::string> *loglevel_Arg;  ///< Runtime log level
  TCLAP::MultiSwitchArg *verbose_Arg;  ///< Cumulative verbose flag to set log level
  TCLAP::ValueArg<std::string> *tiles_directory_Arg;  ///< Output directory for downloaded tiles
  TCLAP::ValueArg<std::string> *output_filename_Arg;  ///< Output filename of the rendered image
  TCLAP::ValueArg<uint16_t> *zoom_Arg;  ///< Zoom level, 0 for autozoom [0..16]
  TCLAP::ValueArg<uint16_t> *max_number_tiles_autozoom_Arg;  ///< Maximum number of tiles in autozoom mode
  TCLAP::ValueArg<uint16_t> *bordertiles_Arg;  ///< Additionnal border tiles. Unit is `tile`
  TCLAP::ValueArg<uint16_t> *cutborder_Arg;  ///< Cut final image to have N number of pixel around drawn tracks [0..2048]
  TCLAP::ValueArg<uint16_t> *waypoint_radius_Arg;  ///< Radius, in pixel, of the circle waypoint
  TCLAP::ValueArg<cli::HTMLColorTrait> *waypoint_color_Arg;  ///< Color (#rrggbbaa) used to draw the stroke of the WayPoint
  TCLAP::ValueArg<uint16_t> *linewidth_Arg;  ///< Stroke width for the track line
  TCLAP::SwitchArg *animate_Arg;  ///< Create images sequence based on tracks
  TCLAP::ValuesConstraint<std::string> *allowed_tile_providers;  ///< TCLAP Container for allowed tiles providers name
  TCLAP::ValueArg<std::string> *tiles_Arg;  ///< Tiles provider
  TCLAP::ValuesConstraint<std::string> *allowed_backgroundpostprocess;  ///< TCLAP Container for allowed processing effects
  TCLAP::ValueArg<std::string> *backgroundpostprocess_Arg;  ///< Post processing effect for background image
  TCLAP::SwitchArg *sparse_Arg;  ///< Draw only track touched tiles
  TCLAP::MultiArg<std::string> *thumbnails_Arg;  ///< List of thumbnails to draw
  TCLAP::ValueArg<uint16_t> *thumbnailsize_Arg;  ///< Size of thumbnail photo
  TCLAP::MultiArg<gps::WayPoint> *invisiblewaypoints_Arg;  ///< Add WayPoint that are invisible
  TCLAP::ValueArg<gps::BoundingBox> *boundingbox_Arg;  ///< Bounding box to draw

  // Constraint pointers
  cli::IntegerConstraint<std::uint16_t> *zoom_level_Constraint;  ///< Zoom level constraint
  cli::IntegerConstraint<std::uint16_t> *max_number_tiles_autozoom_Constraint;  ///< Maximum number of tiles constraint
  cli::IntegerConstraint<std::uint16_t> *bordertiles_Constraint;  ///< Constraint for the margin of tiles
  cli::IntegerConstraint<std::uint16_t> *cutborder_Constraint;  ///< Constraint for the amount of pixel to cut on border
  cli::IntegerConstraint<std::uint16_t> *waypoint_radius_Constraint;  ///< Radius constraint
  cli::IntegerConstraint<std::uint16_t> *linewidth_Constraint;  ///< Line width constraint

  /** \brief Default constructor
   *
   * This constructor is not pubicly accessible. Use the instance()
   * member function instead to access the singleton
   */
  Analyzer();

  /** \brief Default destructor
   *
   * \note Please deleteInstance() before destruct
   */
  ~Analyzer();

  /** \brief Compute the log level
   *
   * Based on the several flags that can define the log level, compute
   * the definitive value
   */
  void evaluate_loglevel();

 public:
  /** \brief Obtain singleton as pointer
   */
  static Analyzer *instance();

  /** \brief Remove singleton
   */
  static void deleteInstance();

  /** \brief Wrapper to TCLAP::CmdLine::setExceptionHandling()
   *
   * \param[in] _state Should CmdLine handle parsing exceptions internally?
   *
   * \note This is a convenience to help catching exceptions when
   * running tests.
   */
  void setExceptionHandling(const bool _state);

  /** \brief Analyze CLI and values accordingly
   *
   * Vector input version.
   */
  void parse(std::vector<std::string> &_argv);

  /** \brief Analyze CLI and values accordingly
   *
   * Argc/Argv input version
   */
  void parse(int _argc, const char *const *_argv);

  /** \brief Print out summary
   */
  void summary() const;

  /** \brief Get the user defined tiles directory
   *
   * \return The tiles directory path name
   */
  std::string getTilesDirectory();

  /** \brief Get the user defined output saving filename
   *
   * \return The filename
   */
  std::string getOutputFilename();

  /** \brief Get the user defined zoom level
   *
   * \return The zoom level as \c uint16_t
   */
  std::uint16_t getZoom();

  /** \brief Get the user defined maximum number of tiles in autozoom
   * mode flag value
   *
   * \return The max_number_tiles_autozoom flag value as \c uint16_t
   */
  std::uint16_t getMaxNumberTilesAutozoom();

  /** \brief Get the user defined bordertiles flag value
   *
   * \return The bordertiles flag value as \c uint16_t
   */
  std::uint16_t getBorderTiles();

  /** \brief Get the user defined cutborder flag value
   *
   * \return The cutborder flag value as \c uint16_t
   */
  std::uint16_t getCutBorder();

  /** \brief Get the user defined waypoint circle radius value
   *
   * \return The waypoint circle radius value as \c std::uint16_t
   */
  std::uint16_t getWaypointRadius();

  /** \brief Get the user defined wapoint color flag value
   *
   * \return The waypoint color flag value as \c std::string
   */
  std::string getWayPointColor();

  /** \brief Get the user defined linewidth flag value
   *
   * \return The linewidth flag value as \c uint16_t
   */
  std::uint16_t getLineWidth();

  /** \brief Get the user defined animate flag
   *
   * \return The animate flag state as \c bool
   */
  bool getAnimate();

  /** \brief Get the user defined tiles flag value
   *
   * \return The tiles flag value as \c std::string
   */
  std::string getTiles();

  /** \brief Get the user defined backgroundpostprocess flag value
   *
   * \return The backgroundpostprocess flag value as \c std::string
   */
  std::string getBackgroundPostProcess();

  /** \brief Get the user defined sparse flag
   *
   * \return The sparse flag state as \c bool
   */
  bool getSparse();

  /** \brief Get the user defined thumbnails vector
   *
   * \return The thumbnails vector as \c std::vector<std::string>
   */
  std::vector<std::string> getThumbnails();

  /** \brief Get the user defined thumbanilsize flag
   *
   * \return The thumbnailsize flag value as \c uint16_t
   */
  std::uint16_t getThumbnailSize();

  /** \brief Get the user defined invisiblewaypoints vector
   *
   * \return The invisiblewaypoints vector as \c std::vector<WayPoint>
   */
  std::vector<gps::WayPoint> getInvisibleWayPoints();

  /** \brief Get the user defined boundingbox
   *
   * \return The boundingboxes vector as \c gps::BoundingBox
   */
  gps::BoundingBox getBoundingBox();
};
}

#endif  // GPX2PNG_SRC_ANALYZER_H_
