/* This file is part of the 'gpx2png' program
 * (https://framagit.org/groolot/gpx2png-ng)
 *
 * Copyright (C) 2021 Grégory David <groolot@groolot.net>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
*/

#ifndef GPX2PNG_SRC_ANALYZERCONSTRAINTS_H_
#define GPX2PNG_SRC_ANALYZERCONSTRAINTS_H_

#include <cstdint>

#include <log4cxx/logger.h>
#include <tclap/Constraint.h>

#include "config.h"

namespace cli {
/** \class IntegerConstraint
 *
 * This is class implement the TCLAP::Constraint interface
 */
template<typename T>
class IntegerConstraint : public TCLAP::Constraint<T>
{
 private:
  T min;  ///< Minimum value allowed for the integer
  T max;  ///< Maximum value allowed for the interger

 public:
  /** \brief Default constructor
   *
   * \param[in] _min Minimum value allowed for the integer
   *
   * \param[in] _max Maximum value allowed for the interger
   */
  explicit IntegerConstraint(T _min,
                             T _max) :
    min(_min),
    max(_max)
  {};

  /** \brief Destructor
   */
  ~IntegerConstraint() {};

  /** \brief Returns a description of the Constraint
   */
  std::string description() const
  {
    std::ostringstream output;
    output << "integer value must be in [" << min << ".." << max << "]";
    return output.str();
  };

  /** \brief Returns the short ID for the Constraint
   */
  std::string shortID() const
  {
    std::ostringstream output;
    output << "INTEGER [" << min << ".." << max << "]";
    return output.str();
  };

  /** \brief Compute checking
   *
   * The method used to verify that the value parsed from the command
   * line meets the constraint.
   *
    \param[in] value The value that will be checked
   */
  bool check(const T &value) const
  {
    return (value >= min) and (value <= max);
  };
};
}

#endif  // GPX2PNG_SRC_ANALYZERCONSTRAINTS_H_
