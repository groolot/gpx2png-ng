/* This file is part of the 'gpx2png' program
 * (https://framagit.org/groolot/gpx2png-ng)
 *
 * Copyright (C) 2021 Grégory David <groolot@groolot.net>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
*/

#ifndef GPX2PNG_SRC_GPS_H_
#define GPX2PNG_SRC_GPS_H_

#include <string>
#include <cstdint>
#include <tclap/ArgException.h>

#include "config.h"

namespace gps {
/** \class Limite
 *
 */
class Limite : public TCLAP::ArgParseException
{
 public:
  double overload;
  explicit Limite(double _v, double _min, double _max, const std::string &_id) :
    TCLAP::ArgParseException("value '" + std::to_string(_v) + "' out of [" + std::to_string(_min) + ".." + std::to_string(_max) + "]", _id),
    overload(_v)
  {};
};

/** \class LatitudeLimite
 *
 */
class LatitudeLimite : public Limite
{
 public:
  explicit LatitudeLimite(double _v) :
    Limite(_v, -90.0, 90.0, "Latitude")
  {};
};

/** \class LongitudeLimite
 *
 */
class LongitudeLimite : public Limite
{
 public:
  explicit LongitudeLimite(double _v) :
    Limite(_v, -180.0, 180.0, "Longitude")
  {};
};

/** \class Coordinate
 *
 */
class Coordinate
{
 public:
  Coordinate();
  explicit Coordinate(double, double);
  Coordinate(const Coordinate &);
  ~Coordinate();
  gps::Coordinate &operator =(const gps::Coordinate &coord);
  friend bool operator ==(const gps::Coordinate &l,
                          const gps::Coordinate &r)
  {
    return std::int32_t(l.latitude_ * 1000000) == std::int32_t(r.latitude_ * 1000000)
           and
           std::int32_t(l.longitude_ * 1000000) == std::int32_t(r.longitude_ * 1000000);
  };
  friend bool operator <(const gps::Coordinate &l,
                         const gps::Coordinate &r)
  {
    return l.longitude_
           <
           r.longitude_;
  };
  friend bool operator >(const gps::Coordinate &l,
                         const gps::Coordinate &r)
  {
    return l.longitude_
           >
           r.longitude_;
  };
  friend bool operator <=(const gps::Coordinate &l,
                          const gps::Coordinate &r)
  {
    return l.longitude_ < r.longitude_
           or
           l == r;
  };
  friend bool operator >=(const gps::Coordinate &l,
                          const gps::Coordinate &r)
  {
    return l.longitude_ > r.longitude_
           or
           l == r;
  };
  std::string str();
  double latitude();
  void latitude(double _v);
  double longitude();
  void longitude(double _v);

 protected:
  double latitude_;
  double longitude_;
};
}

#endif  // GPX2PNG_SRC_GPS_H_
