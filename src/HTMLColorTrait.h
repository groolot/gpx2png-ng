/* This file is part of the 'gpx2png' program
 * (https://framagit.org/groolot/gpx2png-ng)
 *
 * Copyright (C) 2021 Grégory David <groolot@groolot.net>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
*/

#ifndef GPX2PNG_SRC_HTMLCOLORTRAIT_H_
#define GPX2PNG_SRC_HTMLCOLORTRAIT_H_

#include <string>
#include <tclap/ArgTraits.h>

namespace cli {
/** \class HTMLColorTrait
 *
 */
class HTMLColorTrait :
  public TCLAP::StringLikeTrait,
  public std::string
{
 public:
  HTMLColorTrait();
  explicit HTMLColorTrait(std::string c_);
  ~HTMLColorTrait();
  std::string operator =(const std::string &input_string);
};
}

#endif  // GPX2PNG_SRC_HTMLCOLORTRAIT_H_
