/* This file is part of the 'gpx2png' program
 * (https://framagit.org/groolot/gpx2png-ng)
 *
 * Copyright (C) 2021 Grégory David <groolot@groolot.net>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
*/

#ifndef GPX2PNG_SRC_WAYPOINT_H_
#define GPX2PNG_SRC_WAYPOINT_H_

#include "Coordinate.h"

#include <string>

#include <log4cxx/logger.h>
#include <tclap/ArgTraits.h>

namespace gps {
/** \class WayPoint
 *
 */
class WayPoint :
  public TCLAP::StringLikeTrait,
  public gps::Coordinate
{
 public:
  WayPoint();
  WayPoint(double _lat, double _lon);
  WayPoint(const gps::WayPoint &wpt);
  ~WayPoint();
  WayPoint &operator =(const std::string &input_string);
  std::string summary();
};
}

#endif  // GPX2PNG_SRC_WAYPOINT_H_
