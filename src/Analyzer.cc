/* This file is part of the 'gpx2png' program
 * (https://framagit.org/groolot/gpx2png-ng)
 *
 * Copyright (C) 2021 Grégory David <groolot@groolot.net>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
*/

#include "Analyzer.h"
#include "AnalyzerConstraints.h"

#include <ostream>

#include <log4cxx/logger.h>
#include <log4cxx/logmanager.h>
#include <log4cxx/basicconfigurator.h>

#include "config.h"

static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger(PACKAGE));

cli::Analyzer *cli::Analyzer::singleton = nullptr;

cli::Analyzer *cli::Analyzer::instance()
{
  if(!singleton)
  {
    singleton = new Analyzer();
  }

  return singleton;
}

void cli::Analyzer::deleteInstance()
{
  delete singleton;
}

void cli::Analyzer::setExceptionHandling(const bool _state)
{
  cmd.setExceptionHandling(_state);
}

cli::Analyzer::Analyzer() :
  cmd("Draw GPX tracks on a background map based on online tiles (OSM, OpenTopoMap, etc.)",
      ' ',
#ifdef PACKAGE_VERSION
      PACKAGE_VERSION)
#else
      "local-version")
#endif
{
  if(!log4cxx::LogManager::getLoggerRepository()->isConfigured())
  {
    log4cxx::BasicConfigurator::configure();
  }

  logger->setLevel(log4cxx::Level::toLevel(DEFAULT_LOGLEVEL));
  cmd.setExceptionHandling(true);
  // Options and flags
  // -q/--quiet
  quiet_Arg = new TCLAP::SwitchArg(
    "q",
    "quiet",
    "Quiet mode. Do not print output log. The default loglevel. Take precedence over --loglevel flag",
    cmd,
    false);
  // -l/--loglevel
  std::vector<std::string> loglevels;
  loglevels.push_back(log4cxx::Level::getDebug()->toString());
  loglevels.push_back(log4cxx::Level::getInfo()->toString());
  loglevels.push_back(log4cxx::Level::getWarn()->toString());
  loglevels.push_back(log4cxx::Level::getError()->toString());
  loglevels.push_back(log4cxx::Level::getFatal()->toString());
  allowed_loglevels = new TCLAP::ValuesConstraint<std::string>(loglevels);
  loglevel_Arg = new TCLAP::ValueArg<std::string>(
    "l",
    "loglevel",
    "Loglevel, default to " + logger->getLevel()->toString() + ". Take precedence over --verbose flag.",
    false,
    logger->getLevel()->toString(),
    allowed_loglevels,
    cmd);
  // -v/--verbose cumulative
  verbose_Arg = new TCLAP::MultiSwitchArg(
    "v",
    "verbose",
    "Cumulative verbosity. The more you use it, the more the loglevel is verbose (until 3 times). It is the less prioritary loglevel flag against --quiet then --loglevel.",
    cmd);
  // -T/--tilesdir
  tiles_directory_Arg = new TCLAP::ValueArg<std::string>(
    "T",
    "tilesdir",
    "Tiles directory to save downloaded tiles. Default: " + std::string(DEFAULT_TILES_DIRECTORY),
    false,
    std::string(DEFAULT_TILES_DIRECTORY),
    "PATH",
    cmd);
  // -o/--output
  output_filename_Arg = new TCLAP::ValueArg<std::string>(
    "o",
    "output",
    "Rendered image output filename. Default: " + std::string(DEFAULT_OUTPUT_FILENAME),
    false,
    std::string(DEFAULT_OUTPUT_FILENAME),
    "FILENAME",
    cmd);
  // -z/--zoom
  zoom_level_Constraint = new cli::IntegerConstraint<std::uint16_t>(DEFAULT_ZOOM_MIN, DEFAULT_ZOOM_MAX);
  zoom_Arg = new TCLAP::ValueArg<uint16_t>(
    "z",
    "zoom",
    "Zoom level, 0 means automatic zooming. Default: " + std::to_string(DEFAULT_ZOOM),
    false,
    DEFAULT_ZOOM,
    zoom_level_Constraint,
    cmd);
  // -a/--autozoom
  max_number_tiles_autozoom_Constraint = new cli::IntegerConstraint<std::uint16_t>(1, DEFAULT_MAX_NUMBER_TILES_AUTOZOOM_MAX);
  max_number_tiles_autozoom_Arg = new TCLAP::ValueArg<uint16_t>(
    "a",
    "autozoom",
    "Do not use more than N tiles to draw tracks. Default: " + std::to_string(DEFAULT_MAX_NUMBER_TILES_AUTOZOOM),
    false,
    DEFAULT_MAX_NUMBER_TILES_AUTOZOOM,
    max_number_tiles_autozoom_Constraint,
    cmd);
  // -b/--bordertiles
  bordertiles_Constraint = new cli::IntegerConstraint<std::uint16_t>(0, DEFAULT_BORDERTILES_MAX);
  bordertiles_Arg = new TCLAP::ValueArg<uint16_t>(
    "b",
    "bordertiles",
    "Additional map image tiles around the map. Default: " + std::to_string(DEFAULT_BORDERTILES),
    false,
    DEFAULT_BORDERTILES,
    bordertiles_Constraint,
    cmd);
  // -c/--cutborder
  cutborder_Constraint = new cli::IntegerConstraint<std::uint16_t>(DEFAULT_CUTBORDER_MIN, DEFAULT_CUTBORDER_MAX);
  cutborder_Arg = new TCLAP::ValueArg<uint16_t>(
    "c",
    "cutborder",
    "Cut final map to have N pixels around the drawn tracks. Default: " + std::to_string(DEFAULT_CUTBORDER),
    false,
    DEFAULT_CUTBORDER,
    cutborder_Constraint,
    cmd);
  // -r/--waypointradius
  waypoint_radius_Constraint = new cli::IntegerConstraint<std::uint16_t>(0, DEFAULT_WAYPOINT_RADIUS_MAX);
  waypoint_radius_Arg = new TCLAP::ValueArg<uint16_t>(
    "r",
    "waypointradius",
    "Waypoint circle radius. 0 for automatic radius. Default: " + std::to_string(DEFAULT_WAYPOINT_RADIUS),
    false,
    DEFAULT_WAYPOINT_RADIUS,
    waypoint_radius_Constraint,
    cmd);
  // --waypointcolor
  waypoint_color_Arg = new TCLAP::ValueArg<cli::HTMLColorTrait>(
    "",
    "waypointcolor",
    "Waypoint color. Default: " + std::string(DEFAULT_WAYPOINT_COLOR),
    false,
    cli::HTMLColorTrait(),
    "#rrggbb[aa]",
    cmd);
  // -w/--linewidth
  linewidth_Constraint = new cli::IntegerConstraint<std::uint16_t>(DEFAULT_LINE_WIDTH_MIN, DEFAULT_LINE_WIDTH_MAX);
  linewidth_Arg = new TCLAP::ValueArg<uint16_t>(
    "w",
    "linewidth",
    "Stroke line width. Default: " + std::to_string(DEFAULT_LINE_WIDTH),
    false,
    DEFAULT_LINE_WIDTH,
    linewidth_Constraint,
    cmd);
  // -A/--animate
  animate_Arg = new TCLAP::SwitchArg(
    "A",
    "animate",
    "Create animation steps by saving individual images for each drawn track.",
    cmd,
    false);
  // -t/--tiles
  std::vector<std::string> tile_providers;
  tile_providers.push_back("cyclemap");
  tile_providers.push_back("hikebike");
  tile_providers.push_back("hillshading");
  tile_providers.push_back("mapquest");
  tile_providers.push_back("opentopo");
  tile_providers.push_back("opvnkarte");
  tile_providers.push_back("standard");
  tile_providers.push_back("thunderforest");
  tile_providers.push_back("toner");
  tile_providers.push_back("toner-lines");
  tile_providers.push_back("transparent");
  tile_providers.push_back("transport");
  tile_providers.push_back("watercolor");
  tile_providers.push_back("white");
  allowed_tile_providers = new TCLAP::ValuesConstraint<std::string>(tile_providers);
  tiles_Arg = new TCLAP::ValueArg<std::string>(
    "t",
    "tiles",
    "Select the provider of image tiles. Default: " + std::string(DEFAULT_TILES),
    false,
    DEFAULT_TILES,
    allowed_tile_providers,
    cmd);
  // --backgroundpostprocess
  std::vector<std::string> background_processings;
  background_processings.push_back("bright");
  background_processings.push_back("brightgrey");
  background_processings.push_back("grey");
  background_processings.push_back("normal");
  allowed_backgroundpostprocess = new TCLAP::ValuesConstraint<std::string>(background_processings);
  backgroundpostprocess_Arg = new TCLAP::ValueArg<std::string>(
    "", "backgroundpostprocess",
    "Post-process the background (e.g. tiles) by changing saturation and brightness. Default: " + std::string(DEFAULT_BACKGROUND_POST_PROCESS),
    false,
    DEFAULT_BACKGROUND_POST_PROCESS,
    allowed_backgroundpostprocess,
    cmd);
  // -s/--sparse
  sparse_Arg = new TCLAP::SwitchArg(
    "s",
    "sparse",
    "Include only tiles touched by GPS tracks.",
    cmd,
    false);
  // -j/--thumbnail
  thumbnails_Arg = new TCLAP::MultiArg<std::string>(
    "j",
    "thumbnail",
    "Show thumbnail of JPEG photo at the position as determined by GPS coordinates in EXIF tag.",
    false,
    "PATH",
    cmd);
  // -J/--thumbnailsize
  thumbnailsize_Arg = new TCLAP::ValueArg<uint16_t>(
    "J",
    "thumbnailsize",
    "Set size of JPEG photo thumbnails. Default: " + std::to_string(DEFAULT_THUMBNAIL_SIZE),
    false,
    DEFAULT_THUMBNAIL_SIZE,
    "INTEGER",
    cmd);
  // -W/--invisiblewaypoint
  invisiblewaypoints_Arg = new TCLAP::MultiArg<gps::WayPoint>(
    "W",
    "invisiblewaypoint",
    "Additional invisible GPS point to be included in the map. Note that Lat[-90..90] and Lon[-180..180] and format: ddd.ddd:ddd.ddd",
    false,
    "STRING",
    cmd);
  // -B/--boundingbox
  boundingbox_Arg = new TCLAP::ValueArg<gps::BoundingBox>(
    "B",
    "boundingbox",
    "Enforce a bounding box defined with GPS coordinates. Note that Lat[-90..90] and Lon[-180..180] and format: ddd.ddd:ddd.ddd:ddd.ddd:ddd.ddd",
    false,
    gps::BoundingBox(),
    "STRING",
    cmd);
}

cli::Analyzer::~Analyzer()
{
  singleton = nullptr;
}

void cli::Analyzer::evaluate_loglevel()
{
  // Get the value parsed by each arg.
  if(quiet_Arg->isSet() && quiet_Arg->getValue() == true)
  {
    logger->setLevel(log4cxx::Level::getFatal());
  }
  else if(loglevel_Arg->isSet())
  {
    logger->setLevel(log4cxx::Level::toLevel(loglevel_Arg->getValue()));
  }
  else if(verbose_Arg->isSet())
  {
    int verbose_loglevel;

    if(verbose_Arg->getValue() > 3)
    {
      verbose_loglevel = 3;
    }
    else
    {
      verbose_loglevel = verbose_Arg->getValue();
    }

    logger->setLevel(log4cxx::Level::toLevel(std::abs(verbose_loglevel - 4) * 10000));
  }
}

void cli::Analyzer::parse(std::vector<std::string> &_argv)
{
  // Wrapper to TCLAP::CmdLine::parse()
  cmd.parse(_argv);
  this->evaluate_loglevel();
}

void cli::Analyzer::parse(int _argc, const char *const *_argv)
{
  std::vector<std::string> args;

  for(int i = 0; i < _argc; i++)
  {
    args.push_back(_argv[i]);
  }

  this->parse(args);
}

void cli::Analyzer::summary() const
{
  std::ostringstream output;
  output << std::endl << std::boolalpha
         << "[Arguments and flags summary]" << std::endl
         << "  LogLevel: " << logger->getLevel()->toString() << std::endl
         << "  Tiles directory: " << tiles_directory_Arg->getValue() << std::endl
         << "  Output filename: " << output_filename_Arg->getValue() << std::endl
         << "  Zoom level: " << zoom_Arg->getValue() << (zoom_Arg->getValue() == 0 ? " (autozoom)" : "") << std::endl
         << "  Maximum number of tiles in autozoom mode: " << max_number_tiles_autozoom_Arg->getValue() << std::endl
         << "  Bordertiles: " << bordertiles_Arg->getValue() << std::endl
         << "  Cutborder: " << cutborder_Arg->getValue() << std::endl
         << "  WayPoint Radius: " << waypoint_radius_Arg->getValue() << std::endl
         << "  WayPoint Color: " << waypoint_color_Arg->getValue() << std::endl
         << "  Linewidth: " << linewidth_Arg->getValue() << std::endl
         << "  Animate: " << animate_Arg->getValue() << std::endl
         << "  Tiles provider: " << tiles_Arg->getValue() << std::endl
         << "  Backgroundpostprocess: " << backgroundpostprocess_Arg->getValue() << std::endl
         << "  Sparse: " << sparse_Arg->getValue() << std::endl
         << "  Thumbnailsize: " << thumbnailsize_Arg->getValue() << std::endl
         ;

  if(thumbnails_Arg->isSet())
  {
    output << "  Thumbnails: " << std::endl;

    for(auto thumb : *thumbnails_Arg)
    {
      // output << "    " << thumb.summary() << std::endl;
    }
  }

  if(invisiblewaypoints_Arg->isSet())
  {
    output << "  WayPoints: " << std::endl;

    for(auto wpt : *invisiblewaypoints_Arg)
    {
      output << "    " << wpt.summary() << std::endl;
    }
  }

  output << "  BoundingBox: "
         << const_cast<gps::BoundingBox &>(boundingbox_Arg->getValue()).str()
         << std::endl;
  logger->info(output.str());
}

std::string cli::Analyzer::getTilesDirectory()
{
  return tiles_directory_Arg->getValue();
}

std::string cli::Analyzer::getOutputFilename()
{
  return output_filename_Arg->getValue();
}

std::uint16_t cli::Analyzer::getZoom()
{
  return zoom_Arg->getValue();
}

std::uint16_t cli::Analyzer::getMaxNumberTilesAutozoom()
{
  return max_number_tiles_autozoom_Arg->getValue();
}

std::uint16_t cli::Analyzer::getBorderTiles()
{
  return bordertiles_Arg->getValue();
}

std::uint16_t cli::Analyzer::getCutBorder()
{
  return cutborder_Arg->getValue();
}

std::uint16_t cli::Analyzer::getWaypointRadius()
{
  return waypoint_radius_Arg->getValue();
}

std::string cli::Analyzer::getWayPointColor()
{
  return waypoint_color_Arg->getValue();
}

std::uint16_t cli::Analyzer::getLineWidth()
{
  return linewidth_Arg->getValue();
}

bool cli::Analyzer::getAnimate()
{
  return animate_Arg->getValue();
}

std::string cli::Analyzer::getTiles()
{
  return tiles_Arg->getValue();
}

std::string cli::Analyzer::getBackgroundPostProcess()
{
  return backgroundpostprocess_Arg->getValue();
}

bool cli::Analyzer::getSparse()
{
  return sparse_Arg->getValue();
}

std::vector<std::string> cli::Analyzer::getThumbnails()
{
  return thumbnails_Arg->getValue();
}

std::uint16_t cli::Analyzer::getThumbnailSize()
{
  return thumbnailsize_Arg->getValue();
}

std::vector<gps::WayPoint> cli::Analyzer::getInvisibleWayPoints()
{
  return invisiblewaypoints_Arg->getValue();
}

gps::BoundingBox cli::Analyzer::getBoundingBox()
{
  return boundingbox_Arg->getValue();
}
