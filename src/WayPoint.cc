/* This file is part of the 'gpx2png' program
 * (https://framagit.org/groolot/gpx2png-ng)
 *
 * Copyright (C) 2021 Grégory David <groolot@groolot.net>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
*/

#include "WayPoint.h"

#include <ostream>
#include <regex>
#include <cfloat>
#include <tclap/ArgException.h>

gps::WayPoint::WayPoint() :
  gps::Coordinate()
{}

gps::WayPoint::WayPoint(double _lat, double _lon) :
  gps::Coordinate(_lat, _lon)
{}

gps::WayPoint::WayPoint(const gps::WayPoint &wpt) :
  gps::Coordinate(wpt.latitude_, wpt.longitude_)
{}

gps::WayPoint::~WayPoint()
{}

gps::WayPoint &gps::WayPoint::operator =(const std::string &input_string)
{
  std::regex waypoint_syntax("^([+-]?(0|[1-9][0-9]*)\\.[0-9]+):([+-]?(0|[1-9][0-9]*)\\.[0-9]+)$");
  std::smatch waypoint_submatches;

  if(std::regex_match(input_string,
                      waypoint_submatches,
                      waypoint_syntax)
    )
  {
    if(waypoint_submatches.size() == 5)
    {
      latitude(std::stod(waypoint_submatches[1].str()));
      longitude(std::stod(waypoint_submatches[3].str()));
    }
  }
  else
  {
    throw TCLAP::ArgParseException(input_string + " does not validate WayPoint format, please provide a ddd.ddd:ddd.ddd form");
  }

  return *this;
}

std::string gps::WayPoint::summary()
{
  std::ostringstream output;
  output << "wpt: ("
         << str()
         << ")"
         ;
  return output.str();
}
