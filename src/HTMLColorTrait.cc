/* This file is part of the 'gpx2png' program
 * (https://framagit.org/groolot/gpx2png-ng)
 *
 * Copyright (C) 2021 Grégory David <groolot@groolot.net>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
*/

#include "HTMLColorTrait.h"

#include <regex>
#include <tclap/ArgException.h>

#include "config.h"

cli::HTMLColorTrait::HTMLColorTrait() :
  std::string(DEFAULT_WAYPOINT_COLOR)
{}

cli::HTMLColorTrait::HTMLColorTrait(std::string c_) :
  std::string(c_)
{}

cli::HTMLColorTrait::~HTMLColorTrait()
{}

std::string cli::HTMLColorTrait::operator =(const std::string &input_string)
{
  std::regex HTMLColorTrait_syntax("^#[[:xdigit:]]{6,8}$");

  if(std::regex_search(input_string,
                       HTMLColorTrait_syntax)
    )
  {
    *static_cast<std::string *>(this) = input_string;
    return input_string;
  }
  else
  {
    throw TCLAP::ArgParseException(input_string + " does not validate the color format, please provide a 3 hexadecimal digit '#ffffff' form (RGB) or 4 hexadecimal digit '#ffffffff' form (RGBA)");
  }
}
