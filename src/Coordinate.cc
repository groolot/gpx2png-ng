/* This file is part of the 'gpx2png' program
 * (https://framagit.org/groolot/gpx2png-ng)
 *
 * Copyright (C) 2021 Grégory David <groolot@groolot.net>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
*/

#include "Coordinate.h"

#include <cfloat>
#include <iomanip>

gps::Coordinate::Coordinate()
{
  latitude(0);
  longitude(0);
}

gps::Coordinate::Coordinate(double _lat, double _lon)
{
  latitude(_lat);
  longitude(_lon);
}

gps::Coordinate::Coordinate(const Coordinate &_coord) :
  gps::Coordinate(_coord.latitude_, _coord.longitude_)
{}

gps::Coordinate::~Coordinate()
{}

gps::Coordinate &gps::Coordinate::operator =(const gps::Coordinate &coord)
{
  latitude(coord.latitude_);
  longitude(coord.longitude_);
  return *this;
}

std::string gps::Coordinate::str()
{
  std::ostringstream output;
  output << std::setprecision(DBL_DIG)
         << latitude_
         << ":"
         << longitude_
         ;
  return output.str();
}

double gps::Coordinate::latitude()
{
  return latitude_;
}

void gps::Coordinate::latitude(double _v)
{
  if(_v >= -90.0 and _v <= 90.0)
  {
    latitude_ = _v;
  }
  else
  {
    throw(gps::LatitudeLimite(_v));
  }
}

double gps::Coordinate::longitude()
{
  return longitude_;
}

void gps::Coordinate::longitude(double _v)
{
  if(_v >= -180.0 and _v <= 180.0)
  {
    longitude_ = _v;
  }
  else
  {
    throw(gps::LongitudeLimite(_v));
  }
}
